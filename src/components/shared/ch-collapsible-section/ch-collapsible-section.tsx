import { Component, Prop, State } from '@stencil/core';

@Component({
  tag: 'ch-collapsible-section',
  styleUrl: 'ch-collapsible-section.scss'
})
export class ChCollapsibleSection {

  @Prop()
  caption: string;

  @State()
  isCollapsed: boolean;

  toggleSection(event: UIEvent) {
    console.log('toggle', event);
    this.isCollapsed = !this.isCollapsed;
  }

  render() {
    return (
      <section class={{
        'ch-collapsible-section': true,
        'ch-collapsible-section--collapsed': this.isCollapsed
      }}>
        <header class="ch-collapsible-section__header" onClick={(event: UIEvent) => this.toggleSection(event)}>
          <div class="ch-collapsible-section__caption">
            <h3 class='ch-collapsible-section__title'>{this.caption}</h3>
            <div class="psl-collapsible-section__controls">
              <i class={{
                'ch-collapsible-section__icon': true,
                'icon--arrow-up': !this.isCollapsed,
                'icon--arrow-down': this.isCollapsed
              }}/>
            </div>
          </div>
          <horizontal-separator/>
        </header>
        <main class="ch-collapsible-section__content">
          <slot/>
        </main>
      </section>
    );
  }
}
