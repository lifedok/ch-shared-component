import { Component } from '@stencil/core';


@Component({
  tag: 'my-app',
  styleUrl: 'my-app.scss'
})
export class MyApp {

  hostData() {
    return {
      'class': 'my-app'
    }
  }

  render() {
    return (
      <div>
        <ch-collapsible-section caption="Title">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit.
          Ab aperiam esse expedita maxime mollitia nemo quae.
          Accusamus aliquam cumque est ipsa, magni molestias perferendis perspiciatis placeat provident quaerat qui sed.
        </ch-collapsible-section>
      </div>
    )
  }
}
