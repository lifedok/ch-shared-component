import { Component } from '@stencil/core';


@Component({
  tag: 'ch-primary-page',
  styleUrl: 'ch-primary-page.scss'
})
export class ChPrimaryPage {

  hostData() {
    return {
      'class': 'ch-primary-page'
    }
  }

  render() {
    return (
      <div>
        <ch-collapsible-section caption="Title">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit.
          Ad cupiditate dignissimos est facere illo magnam possimus sunt vel.
          Ab alias animi atque delectus doloribus eum exercitationem facere nesciunt possimus tempore!
        </ch-collapsible-section>
      </div>
    )
  }
}
